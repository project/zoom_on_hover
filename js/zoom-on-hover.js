(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.zoom = {
    attach: function (context, settings) {
      once('zoom', '.xzoom-container img.xzoom', context).forEach((thisImg) => {
        var original_urls = drupalSettings.imageFieldZoom.image_urls;
        var imgWidth = $(thisImg).width();
        var zoomWidth = imgWidth / 2;
        var image_zoom_style = $(thisImg).data('image-zoom-style');
        var image_position = $(thisImg).data('image-position');
        var defaultscale = $(thisImg).data('defaultscale');
        var scroll = $(thisImg).data('scroll');
        var tint = $(thisImg).data('tint');
        var preview_box_dimension = $(thisImg).data('zoom-preview-dimension');
        var zoomAttr = {};
        if (image_zoom_style == 'background') {
          zoomAttr = {
            defaultScale: defaultscale,
            title: 'TRUE',
            Xoffset: 15,
            position: 'inside',
            scroll: scroll
          };
        } else if (image_zoom_style == 'box') {
          if (image_position == 'inside') {
            zoomAttr = {
              defaultScale: defaultscale,
              position: 'lens',
              lensShape: 'box',
              bg: 'FALSE',
              sourceClass: 'xzoom-hidden',
              tint: tint,
              scroll: scroll
            };
          } else {
            var zoomWidth = $(thisImg).data('preview-width');
            var zoomHeight = $(thisImg).data('preview-height');
            zoomAttr = {
              zoomWidth: zoomWidth,
              zoomHeight: zoomHeight,
              defaultScale: defaultscale,
              position: image_position,
              tint: tint,
              scroll: scroll
            };
          }
        } else {
          zoomAttr = {
            defaultScale: defaultscale,
            position: 'lens',
            lensShape: 'circle',
            sourceClass: 'xzoom-hidden',
            tint: tint,
            scroll: scroll
          };
        }
        $(once('init', thisImg)).xzoom(zoomAttr);
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
