
## Zoom on Hover

Zoom on Hover provides a field formatter that allows you to apply it to
your image field.

### Install

* Install module and enable like any other contrib module.

* Enable the module.

### Usage

* Add an image field to your content type/entity type.

* On the "Manage Display" setting page, select the "Zoom on hover field" 
formatter.

* Click the gear on the right to configure how you want the Image zoom to work.

* You'll see the zoom in action when the images are rendered on the node/entity
pages.


### Maintainers

Sujan Shrestha (sujanshrestha)
https://www.drupal.org/u/sujanshrestha
