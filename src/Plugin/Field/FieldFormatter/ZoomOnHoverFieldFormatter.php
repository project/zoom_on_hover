<?php

namespace Drupal\zoom_on_hover\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'zoomonhover_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "zoomonhover_field_formatter",
 *   label = @Translation("Zoom on hover"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ZoomOnHoverFieldFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Image entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param object $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   Image style storage.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    object $current_user,
    EntityStorageInterface $image_style_storage,
    Renderer $renderer,
    FileUrlGeneratorInterface $fileUrlGenerator
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
    $this->renderer = $renderer;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('renderer'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {

    return [
      'image_style' => '',
      'image_zoom_style' => 'mouseover',
      'image_position' => 'right',
      'preview_box_dimension' => 'same_image_width',
      'box_width' => '200',
      'box_height' => '200',
      'defaultScale' => 0,
      'image_class' => '',
      'image_wrapper_class' => '',
      'scroll' => TRUE,
      'tint' => '',
    ] + parent::defaultSettings();
  }

  /**
   * Settings form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   *
   * @return mixed
   *   Returns mixed data.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    $image_positions = [
      'top' => $this->t('Top'),
      'right' => $this->t('Right'),
      'bottom' => $this->t('Bottom'),
      'right' => $this->t('Right'),
      'inside' => $this->t('Inside'),
    ];

    $image_zoom_styles = [
      'background' => $this->t('zoom image output as background'),
      'box' => $this->t('zoom image output as box'),
      'circle' => $this->t('zoom image output as circle'),
    ];

    $image_zoom_style = $this->getSetting('image_zoom_style') ?? 'box';

    $element['image_zoom_style'] = [
      '#title' => $this->t('Image Zoom Preview Style'),
      '#type' => 'select',
      '#default_value' => $image_zoom_style,
      '#options' => $image_zoom_styles,
    ];

    $image_position = $this->getSetting('image_position') ?? 'right';

    $element['image_position'] = [
      '#title' => $this->t('Image Box Position'),
      '#type' => 'select',
      '#default_value' => $image_position,
      '#options' => $image_positions,
      '#states' => [
        'visible' => [
          ':input[name$="[field_image][settings_edit_form][settings][image_zoom_style]"]' => ['value' => 'box'],
        ],
      ],
    ];

    if ($image_position == 'inside' || $image_zoom_style != 'box') {
      $preview_box_dimension = 'same_image_width';

    }
    else {
      $preview_box_dimension = $this->getSetting('preview_box_dimension') ?? 'same_image_width';

    }

    $element['preview_box_dimension'] = [
      '#title' => $this->t('Preview Box Dimension'),
      '#type' => 'select',
      '#default_value' => $preview_box_dimension,
      '#options' => [
        'same_image_width' => $this->t('Same as image width'),
        'custom' => $this->t('Custom Dimension'),
      ],
      '#states' => [
        'visible' => [
          ':input[name$="[field_image][settings_edit_form][settings][image_zoom_style]"]' => ['value' => 'box'],
        ],
        'invisible' => [
          ':input[name$="[field_image][settings_edit_form][settings][image_position]"]' => ['value' => 'inside'],
        ],
      ],
      '#description' => $this->t('<small>If you select the "Same as image width" options and there are no window space at the "Image Box Position" side then the zoom preview will be inside the image.<br>We can choose custom dimension in that case.</small>'),
    ];

    $element['box_width'] = [
      '#title' => $this->t('Box Width'),
      '#type' => 'number',
      '#min' => '100',
      '#max' => '1200',
      '#default_value' => $this->getSetting('box_width') ?? '200',
      '#states' => [
        'visible' => [
          ':input[name$="[field_image][settings_edit_form][settings][preview_box_dimension]"]' => ['value' => 'custom'],
          ':input[name$="[field_image][settings_edit_form][settings][image_position]"]' => ['!value' => 'inside'],
        ],
      ],
    ];

    $element['box_height'] = [
      '#title' => $this->t('Box Height'),
      '#type' => 'number',
      '#min' => '100',
      '#max' => '1200',
      '#default_value' => $this->getSetting('box_height') ?? '200',
      '#states' => [
        'visible' => [
          ':input[name$="[field_image][settings_edit_form][settings][preview_box_dimension]"]' => ['value' => 'custom'],
          ':input[name$="[field_image][settings_edit_form][settings][image_position]"]' => ['!value' => 'inside'],
        ],
      ],
    ];

    $element['defaultScale'] = [
      '#title' => $this->t('Default Scale'),
      '#type' => 'number',
      '#min' => '0',
      '#max' => '1',
      '#step' => '.1',
      '#default_value' => $this->getSetting('defaultScale') ?? '0',
      '#description' => $this->t('from -1 to 1, that means -100% till 100% scale'),
    ];

    $element['scroll'] = [
      '#title' => $this->t('Zoom on mouse scroll'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('scroll') ?? TRUE,
      '#description' => $this->t('Enable "Zoom on mouse scroll" to zoom-in and zoom out on mouse scroll.'),
    ];

    $element['tint'] = [
      '#title' => $this->t('Tint color'),
      '#type' => 'textfield',
      '#description' => $this->t('Tint color for eg. #282828, red etc.'),
      '#size' => '20',
      '#default_value' => $this->getSetting('tint'),
    ];

    $element['image_class'] = [
      '#title' => $this->t('Image class'),
      '#type' => 'textfield',
      '#description' => $this->t('Custom class for the image'),
      '#size' => '20',
      '#default_value' => $this->getSetting('image_wrapper_class'),
    ];

    $element['image_wrapper_class'] = [
      '#title' => $this->t('Wrapper class'),
      '#type' => 'textfield',
      '#description' => $this->t('Custom class for the image wrapper'),
      '#size' => '20',
      '#default_value' => $this->getSetting('image_wrapper_class'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $image_styles = image_style_options(FALSE);
    unset($image_styles['']);
    $image_style_setting = $this->getSetting('image_style');

    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @value', ['@value' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }
    $image_zoom_style = $this->getSetting('image_zoom_style');
    $summary[] = $this->t('Zoom style: @value', ['@value' => $image_zoom_style]);
    if ($image_zoom_style == 'box') {
      $summary[] = $this->t('Image Zoom Position: @value', ['@value' => $this->getSetting('image_position')]);
    }
    $summary[] = $this->t('Default Scale: @value', ['@value' => $this->getSetting('defaultScale')]);
    $summary[] = $this->t('Zoom on scroll: @value', ['@value' => $this->getSetting('scroll') ? 'true' : 'false']);
    $summary[] = $this->t('Tint color: @value <span  style="background-color:@value; width:10px;height:10px;display: inline-block;margin:0 2px"></span>', ['@value' => $this->getSetting('tint')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $field_name = $items->getFieldDefinition()->getName();
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);
    $images = [];
    $original_urls = [];
    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }
    $image_style_setting = $this->getSetting('image_style');

    // Collect cache tags to be added for each item in the field.
    $cache_tags = [];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      $image_style_url = '';
      $cache_contexts = [];
      $image_uri = $file->getFileUri();

      if (!empty($image_style_setting)) {
        $image_style_url = $this->imageStyleStorage->load($image_style_setting)->buildUrl($image_uri);
      }

      $cache_contexts[] = 'url.site';
      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      if (!empty($item->_attributes)) {
        $item_attributes = $item->_attributes;
      }
      $image_target_id = $item->getValue()['target_id'];

      $image_uri = $this->fileUrlGenerator->generateAbsoluteString($image_uri);

      $image_uri = parse_url($image_uri);

      $image_uri_path = $image_uri['path'] ?? '';

      $original_urls[$image_target_id] = $image_uri_path;

      $item_attributes = [];

      // Adding custom attributes to the img.
      $item_attributes['class'][] = 'xzoom';
      $item_attributes['xoriginal'] = $image_uri_path;
      $item_attributes['fid'] = $image_target_id;
      unset($item->_attributes);

      $images[$delta] = [
        'image_style_url' => ($image_style_url != '') ? $image_style_url : $image_uri_path,
        'image_url' => $image_uri_path,
        'image_zoom_style' => $this->getSetting('image_zoom_style'),
        'image_position' => $this->getSetting('image_position'),

        'preview_box_dimension' => $this->getSetting('preview_box_dimension'),
        'box_width' => $this->getSetting('box_width'),
        'box_height' => $this->getSetting('box_height'),

        'defaultScale' => $this->getSetting('defaultScale'),
        'scroll' => $this->getSetting('scroll'),
        'tint' => $this->getSetting('tint'),
        'image_class' => $this->getSetting('image_class'),
        'image_wrapper_class' => $this->getSetting('image_wrapper_class'),
      ];

    }

    return [
      '#theme' => 'zoom_on_hover',
      '#images' => $images,
      'field_name' => [$field_name],
      '#attached' => [
        'drupalSettings' => [
          'imageFieldZoom' => [
            'image_zoom_style' => $this->getSetting('image_zoom_style'),
            'image_position' => $this->getSetting('image_position'),
            'defaultScale' => $this->getSetting('defaultScale'),
            'scroll' => $this->getSetting('scroll'),
            'tint' => $this->getSetting('tint'),
            'image_urls' => $original_urls,
          ],
        ],
      ],
    ];
  }

}
